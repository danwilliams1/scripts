## Setup
 * Make sure you have perl and carton installed;
 * Checkout the repository somewhere in your DEV folder;
 * Add `export PERL5LIB="/c/DEV/<path to repo>/local/lib/perl5"` to your .bashrc file;
 * Add `export PATH="/c/DEV/<path to repo>:$PATH"` to your .bashrc file;
 * Add `export NAMESPACE="<your namespace>"` to your .bashrc file;
 * Run `source ~/.bashrc`;
 * In the root of the checkout run `carton install`